import { render, screen } from "@testing-library/react";
import { it, expect, describe } from "vitest";
import App from "../src/App";

describe("UI test", () => {
  it("should display the page title", () => {
    render(<App />);
    expect(screen.getByRole("heading")).toBeInTheDocument();
    expect(screen.getByRole("heading")).toHaveTextContent(
      "Teenage Boy Speaking"
    );
  });

  it("should display a text input to type text in it", () => {
    render(<App />);
    expect(screen.getByRole("textbox")).toBeInTheDocument();
  });

  it("should display a button to submit text input", () => {
    render(<App />);
    expect(screen.getByRole("button")).toBeInTheDocument();
  });

});

